﻿#include <iostream>
using namespace std;
const int a = 10;
void PrintDigitV1(int, string);
void PrintDigitV2(bool);

int main() 
{
    int x;
    string y;
    for (int i = 0; i <= a; ++i)
    {
        if (i % 2 == 0)
        {
            cout << i;
        }
    }
    cout << endl;
    cout << endl << "Enter value: ";
    cin >> x;
    cout << "Even or Odd?" << endl;
    cin >> y;
    PrintDigitV1(x,y);
    cout << endl;
    PrintDigitV2(0); // 0 - Четные, 1 - нечетные
}

void PrintDigitV1(int x, string y)
{
    for (int i = 0; i <= x; ++i)
    {
        if (i % 2 != 0 && y == "odd")
        {
            cout << i << " ";
        }
        else if (i % 2 == 0 && y == "even")
        {
            cout << i << " ";
        }
    }
}
void PrintDigitV2(bool x)
{
    for (int i = x; i <= a; i+=2)
    {
        cout << i << " ";
    }
}